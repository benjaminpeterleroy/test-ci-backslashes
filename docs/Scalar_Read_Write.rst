.. aimms:module:: axll



Scalar Read Write
-------------------------

.. toctree::

   
.. aimms:externalprocedure:: ReadSingleValue(ScalarReference,Cell)



    This function reads a cell from the active excel sheet into the given identifier.
    
    The type of the identifier (numerical, string, element) should match with the content of the cell.

    .. aimms:handle:: ScalarReference
    
        :attribute Property: Output
    
    
        (output) The scalar identifier to be changed. This can also be a multi dimensional 
        identifier where all indices are fixed, such that the resulting slice is a scalar.
    
    .. aimms:stringparameter:: Cell
    
        :attribute Property: Input
    
    
        The cell in the active sheet to read from.
        Examples: "A1", "G4" 
    
.. aimms:externalprocedure:: WriteSingleValue(ScalarReference,Cell)



    This function writes a scalar to the active excel sheet 
    
    The type of the identifier (numerical, string, element) determines whether
    the cell will be formatted as a number or as text.

    .. aimms:handle:: ScalarReference
    
        :attribute Property: Input
    
    
        The scalar identifier to be written. This can also be a multi dimensional 
        identifier where all indices are fixed, such that the resulting slice is a scalar.
    
    .. aimms:stringparameter:: Cell
    
        :attribute Property: Input
    
    
        The cell in the active sheet to write to.
        Examples: "A1", "G4\
    
.. aimms:externalprocedure:: WriteFormula(FormulaString,Cell)



    This function creates a formula in the active sheet.
    
    The given string should be a valid formula representation in Excel. It is copied as is.
    
    Please note that the AimmsXLLibrary is not capable of evaluating any formula. 
    It can only read and write .xls or .xlsx files and does not have access to the full
    calculation engine of Excel.
    To evaluate a formula you must open the sheet in Excel. Excel does store the result
    of a formula in the cell and these calculated results of a formula can be
    read back using the AimmsXLLibrary.
    
    **Examples:**
    
        .. code-block:: none

            WriteFormula("=SUM(B2:B6)","B7");
    
            WriteFormula("=HYPERLINK(\\"#B7\\",\\"Goto Sum\\")", "A8");

    .. aimms:stringparameter:: FormulaString
    
        :attribute Property: Input
    
    
    
        A string containing a valid Excel formula.
    
    .. aimms:stringparameter:: Cell
    
        :attribute Property: Input
    
    
    
        The cell in the active sheet to write to.
    