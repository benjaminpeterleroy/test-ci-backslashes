.. aimms:module:: axll



Utilities
-----------------

.. toctree::

   
.. aimms:externalprocedure:: ConstructRange(startCell,width,height,ResultingRange)



    This support function creates a range string given a starting cell and sizes.
    
    **Example:**

        .. code::
        
          ConstructRange("C2",2,10,myString) 
        
      sets myString to "C2:D11" 

    .. aimms:stringparameter:: StartCell
    
        :attribute Property: Input
    
    
        A string representing the top left cell of the range. 
        For example: "A1" or "D15".
    
    .. aimms:parameter:: Width
    
        :attribute Property: Input, Integer
    
    
    
        The number of columns of the range. It should be an integer value >= 1.
    
    .. aimms:parameter:: Height
    
        :attribute Property: Input, Integer
    
    
    
        The number of rows of the range. It should be an integer value >= 1.
    
    .. aimms:stringparameter:: ResultingRange
    
        :attribute Property: Output
    
    
        (Output) The constructed range representation. 
        Example: "C2:D11" 
    
.. aimms:externalprocedure:: GetAllSheetNames(SheetNames)




    This function reads all existing sheet names of the active workbook and adds them as elements to the give set.

    .. aimms:set:: SheetNames
    
        :attribute Property: Output
    
    
        (Output) This argument should refer to an (empty) root set. On return the set will contain elements 
        that are named according to all sheets in the workbook.
    
    
.. aimms:externalprocedure:: GetNamedRanges(RangeNames,SheetName)



    This function reads all the named ranges for the given sheet (both local and global scope).
    The names of the ranges will be added as elements to the given set.

    .. aimms:set:: RangeNames
    
        :attribute Property: Output
    
    
        (Output) This argument should refer to an (empty) root set. On return the set will contain elements 
        that are named according to the named ranges.
    
    
    .. aimms:stringparameter:: SheetName
    
        :attribute Property: Optional
    
    
        (optional) The name of an existing sheet in the active workbook.
        If not specified the active sheet will be used.
    
.. aimms:externalprocedure:: ClearActiveSheet



    This function clears the entire content of the currently active sheet.

.. aimms:externalprocedure:: ClearRange(RangeToClear)




    This function clears all cells in the given range in the currently active sheet.

    .. aimms:stringparameter:: RangeToClear
    
        :attribute Property: Input
    
    
        The (named) range to be cleared.
        Examples: "A3:G10", "MyNamedRange\
    
.. aimms:externalprocedure:: ColumnNumber(colName)


    :attribute ReturnType: integer


    This utility function will return the sequence number of the column passed in.
    
    **For example:**
    
     - ColumnNumber("A") will return 1
    
     - ColumnNumber("B") will return 2
    
     - ColumnNumber("AB") will return 28
    
    The name passed in can only contain characters in the range 'A' to 'Z' (or 'a' to 'z').
    
    Please note that there are limits on the number of columns in Excel:
    The maximum column name for an .xlsx file is "XFD" (16,384) and for an .xls file it is "IV" (256).

    .. aimms:stringparameter:: colName
    
        :attribute Property: Input
    
    
        The name of a column.
        Examples: "A", "AB\
    
.. aimms:externalprocedure:: ColumnName(colNumber,colName)



    This utility function gives you the name that corresponds to the n-th column
    
    **For example:**
    
     - ColumnName(1,name) will set name to "A"
    
     - ColumnName(2,name) will set name to "B"
    
     - ColumnName(28,name) will set name to "AB"
    
    The column number should be an integer greater or equal to 1.
    
    Please note that there are limits on the number of columns in Excel:
    The maximum number of columns an .xlsx file is 16,384 ("XFD") and for an .xls file it is 256 ("IV").

    .. aimms:parameter:: colNumber
    
        :attribute Property: Input
    
    
    
        The column number (should be >= 1)
    
    .. aimms:stringparameter:: colName
    
        :attribute Property: Output
    
    
    
        (output) The name of the column.
    
.. aimms:externalprocedure:: CopyRange(DestinationRange,SourceRange,SourceSheet,AllowRangeOverflow)



    This function will copy all cells in a range to another range within the same workbook. All cell formatting is copied as well.
    
    If copying within the same sheet, it is not allowed to specify ranges that (partly) overlap.
    
    **Example:**
    
      .. code::
        
        CopyRange("B2", "A1:D10", SourceSheet:"OtherSheet", AllowRangeOverflow:1)
    
     This copies all the cells in the range A1:D10 of sheet OtherSheet to the range B2:E11 in the active sheet.

    .. aimms:stringparameter:: DestinationRange
    
        :attribute Property: Input
    
    
    .. aimms:stringparameter:: SourceRange
    
        :attribute Property: Input
    
    
    .. aimms:stringparameter:: SourceSheet
    
        :attribute Property: Optional
    
    
    .. aimms:parameter:: AllowRangeOverflow
    
        :attribute Default: 1
    
        :attribute Property: Optional
    
    
.. aimms:externalprocedure:: FirstUsedRowNumber

    :attribute ReturnType: integer




    This function returns the first row in the current sheet that contains a cell with data.

.. aimms:externalprocedure:: LastUsedRowNumber

    :attribute ReturnType: integer




    This function returns the last row in the current sheet that contains a cell with data.

.. aimms:externalprocedure:: FirstUsedColumnNumber

    :attribute ReturnType: integer



    This function returns the number of the first column in the current sheet that contains a cell with data.
    If you need the corresponding column name you can use the function :any:`ColumnName`.

.. aimms:externalprocedure:: LastUsedColumnNumber

    :attribute ReturnType: integer



    This function returns the number of the last column in the current sheet that contains a cell with data.
    If you need the corresponding column name you can use the function :any:`ColumnName`.

.. aimms:externalprocedure:: SetRangeBackgroundColor(RangeToColor,red,green,blue)




    With this function you can specify a background color for the given cell range.

    .. aimms:stringparameter:: RangeToColor
    
        :attribute Property: Input
    
    
        The (named) range for which you want to specify the background color.
        Examples: "A3:G10", "C1", "MyNamedRange" 
    
    .. aimms:parameter:: red
    
        :attribute Property: Input
    
    
    
        The 'red' value of an RGB color value [0 .. 255]
    
    .. aimms:parameter:: green
    
        :attribute Property: Input
    
    
    
        The 'green' value of an RGB color value [0 .. 255]
    
    .. aimms:parameter:: blue
    
        :attribute Property: Input
    
    
    
        The 'blue' value of an RGB color value [0 .. 255]
    