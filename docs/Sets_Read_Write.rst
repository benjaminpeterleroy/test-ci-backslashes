.. aimms:module:: axll



Sets Read Write
-----------------------

.. toctree::

   
.. aimms:externalprocedure:: WriteSet(SetReference,SetRange,AllowRangeOverflow)



    This function writes the elements of a set to the active Excel sheet.
    
    .. note::
    
      - An error occurs if the range is too small, except when :any:`AllowRangeOverflow` is set to 1.
    
      - Remaining cells are emptied if the there are more cells than set elements.
    
      - When writing a calendar set, the cells will be formatted as Date/Time unless the option :any:`CalendarElementsAsStrings` is set to 1.

    .. aimms:set:: SetReference
    
        :attribute Property: Input
    
    
    
        The (simple) set to be written to excel.
    
    
    .. aimms:stringparameter:: SetRange
    
        :attribute Property: Input
    
    
        The 1 dimensional excel range where the data should be written, either horizontal or vertical.
        
        Examples: "A1:A10" or "B2:M2" 
    
    .. aimms:parameter:: AllowRangeOverflow
    
        :attribute Range: :aimms:set:`[0, 1]`
    
        :attribute Property: Optional
    
    
        optional (default 0): if set to 1 and the cardinality of the set is greater than the size of the range,
        then the write operation is allowed to extend the range to the needed size.
    
.. aimms:externalprocedure:: ReadSet(SetReference,SetRange,ExtendSuperSets,MergeWithExistingElements,SkipEmptyCells)



    This function reads the cells of a range from the active excel sheet and converts them to
    elements in the given set reference.

    .. aimms:set:: SetReference
    
        :attribute Property: InOut
    
    
        The (simple) set to which the elements should be added. 
        If the argument :any:`MergeWithExistingElements` is set to 0, the set will first be emptied.
    
    
    .. aimms:stringparameter:: SetRange
    
        :attribute Property: Input
    
    
        The 1 dimensional excel range where the data resides, either horizontal or vertical.
        
        Examples: "A1:A10" or "B2:M2" 
    
    .. aimms:parameter:: ExtendSuperSets
    
        :attribute Range: :aimms:set:`[0, 2]`
    
        :attribute Property: Input
    
    
        This determines what should happen with elements that are not present in the super set of the given set.
        
        Values:
        
        - 0 : elements not in the parent set result in an error
        
        - 1 : elements not in the parent set are added recursively
        
        - 2 : elements not in the parent set are skipped
        
        If :any:`SetReference` does not refer to a set that has the subsetOf attribute specified, then this argument is ignored.
    
    .. aimms:parameter:: MergeWithExistingElements
    
        :attribute Property: Optional
    
    
        (optional) Default is 0.  
        If this option is set to 1 then the elements from the range are added to the current content of the set.
        If set to 0, the set is first emptied and then the elements are added.
    
    .. aimms:parameter:: SkipEmptyCells
    
        :attribute Property: Optional
    
    
        (optional) Default is 0.
        
        - If set to 0, reading of the range stops as soon as an empty cell is encountered and a warning is raised.
        - If set to 1, an empty cell in the range is simply skipped.
    